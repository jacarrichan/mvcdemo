package snippet;

import java.lang.reflect.Method;

import org.junit.Test;


/**
 * @description 用于测试发射的性能
 * @author jacarri
 * @date 2014年9月23日 上午11:09:10
 * @version v1.0
 */
public class TestReflectPerformance {
    private int num = 10000000;


    @Test
    public void noreflect() {
        Person p = new Person();
        for (int i = 0; i < num; ++i) {
            Person.setName(p, "name");
            Person.setAge(p, "22");
        }
    }


    @Test
    public void reflect() {
        Person p = new Person();
        try {
            Method setname = Person.class.getDeclaredMethod("setName", Person.class, String.class);
            Method setage = Person.class.getDeclaredMethod("setAge", Person.class, String.class);
            for (int i = 0; i < num; ++i) {
                setname.invoke(null, p, "name");
                setage.invoke(null, p, "22");
            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
